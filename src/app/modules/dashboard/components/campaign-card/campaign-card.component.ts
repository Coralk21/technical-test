import { Component, Input, OnInit } from '@angular/core';
import { Campaign } from '../../../../types/models.types';

@Component({
  selector: 'app-campaign-card',
  templateUrl: './campaign-card.component.html',
  styleUrls: ['./campaign-card.component.scss']
})
export class CampaignCardComponent implements OnInit {
  @Input() campaign: Campaign;

  constructor() { }

  ngOnInit(): void {}

}
