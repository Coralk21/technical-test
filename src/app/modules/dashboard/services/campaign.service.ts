import { Injectable } from '@angular/core';
import { Campaign, CampaignStatus } from '../../../types/models.types';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {

  constructor() { }

  getCampaigns(): Campaign[] {
    return [
      {
        title: 'La batalla contra el covid depende del autocuidado',
        status: CampaignStatus.ACTIVE,
        category: {
          name: 'Covid 19',
          color: '#D14F69',
          icon: 'far fa-frown'
        },
        type: {
          name: 'Preventiva',
          color: '#FB9121'
        },
        usersImpacted: Math.floor( 1000 * Math.random() ),
        createdAt: new Date().toLocaleDateString(),
        updateAt: new Date().toLocaleDateString()
      },
      {
        title: 'Campana 2',
        status: CampaignStatus.ACTIVE,
        category: {
          name: 'Embarazo',
          color: '#DA8CF9',
          icon: 'fas fa-female'
        },
        type: {
          name: 'Preventiva',
          color: '#FB9121'
        },
        usersImpacted: Math.floor( 1000 * Math.random() ),
        createdAt: new Date().toLocaleDateString(),
        updateAt: new Date().toLocaleDateString()
      },
      {
        title: 'Campana 3',
        status: CampaignStatus.ACTIVE,
        category: {
          name: 'Nutrición',
          color: '#76955A',
          icon: 'fas fa-utensils'
        },
        type: {
          name: 'Preventiva',
          color: '#FB9121'
        },
        usersImpacted: Math.floor( 1000 * Math.random() ),
        createdAt: new Date().toLocaleDateString(),
        updateAt: new Date().toLocaleDateString()
      }
    ];
  }
}
