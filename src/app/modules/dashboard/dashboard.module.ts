import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { CampaignCardComponent } from './components/campaign-card/campaign-card.component';
import { CampaignService } from './services/campaign.service';


@NgModule({
  declarations: [DashboardComponent, CampaignCardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule
  ],
  providers: [
    CampaignService
  ]
})
export class DashboardModule { }
