interface IRoute {
  path: string;
  icon: string;
  label: string;
}

export {
  IRoute
};
