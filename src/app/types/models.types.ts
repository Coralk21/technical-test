enum CampaignStatus {
  ACTIVE,
  INACTIVE
}

interface CampaignType {
  name: string;
  color: string;
}

interface CampaignCategory {
  name: string;
  icon: string;
  color: string;
}

interface Campaign {
  title: string;
  category: CampaignCategory;
  type: CampaignType;
  status: CampaignStatus;
  usersImpacted: number;
  createdAt: string;
  updateAt: string;
}

export {
  CampaignStatus,
  CampaignType,
  Campaign
};
