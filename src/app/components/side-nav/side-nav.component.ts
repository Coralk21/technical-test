import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IRoute } from '../../types/app.types';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {
  @Output() openedChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() opened = false;

  routes: IRoute[] = [
    {
      path: '/dashboard',
      icon: 'fas fa-chart-line',
      label: 'Dashboard'
    },
    {
      path: '/example',
      icon: 'fas fa-address-book',
      label: 'Contacts'
    },
    {
      path: '/example',
      icon: 'fas fa-columns',
      label: 'Builder'
    },
    {
      path: '/example',
      icon: 'fas fa-atlas',
      label: 'Maps'
    },
    {
      path: '/example',
      icon: 'fas fa-battle-net',
      label: 'Help'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

  toggleSideNav(): void {
    this.opened = !this.opened;
    this.openedChange.emit(this.opened);
  }

}
