import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() toggleMenu: EventEmitter<void> = new EventEmitter<void>();
  notifications: any[] = [
    { icon: 'fas fa-bell', number: 30 },
    { icon: 'fas fa-reply', number: 30 },
    { icon: 'fas fa-phone', number: 30 },
    { icon: 'fas fa-mobile-alt', number: 30 },
    { icon: 'fas fa-envelope', number: 30 },
  ];

  constructor() { }

  ngOnInit(): void {
  }

  emitToggleMenu(): void {
    this.toggleMenu.emit();
  }

}
